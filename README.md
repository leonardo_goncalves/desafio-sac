# SAC

This project is an OLX challenge for the selective process. Its objective is register an user call and list all user calls.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To run this project you will need to install:

* Java 8 or superior
* Apache Maven 3.0.0 or superior
* MySQL 5.6.0. or superior

### Installing

Clone this repository into your local enviroment.
```
git clone https://leonardo_goncalves@bitbucket.org/leonardo_goncalves/desafio-sac.git
```
### Setting up Database

Install an instance of MySQL database and run the following scripts.

* [schema-mysql.sql](https://bitbucket.org/leonardo_goncalves/desafio-sac/src/master/src/main/resources/schema-mysql.sql?fileviewer=file-view-default) - Create database schema, create application user and set its privileges.
* [data.sql](https://bitbucket.org/leonardo_goncalves/desafio-sac/src/master/src/main/resources/data.sql?fileviewer=file-view-default) - Populate database with some data

**Attention**
If your database is installed in another host, you can set up the correct connection url through the property **spring.datasource.url** in [application.properties](https://bitbucket.org/leonardo_goncalves/desafio-sac/src/master/src/main/resources/application.properties?fileviewer=file-view-default#application.properties-10)

### Running

For the next commands you will need to have Maven in your system environment variable. To run the project, perform the following command:

```
mvn spring-boot:run
```

The application will startup and you can access through the url:

```
http://localhost:8080/sac
```

## Running the tests

For run the tests, perform the following command:

```
mvn test
```

## Deployment

For deployment, perform the following command:

```
mvn package
```

It will run the tests and package the application at **target\sac.jar**.
The generated artifact can be deployed anywhere.
**Don't forget to set up the database for production environment, see this in the Setting up Database section.**
To run the project from the generated artifact, perform the following command:
```
java -jar sac.jar
```

## Built With

* [Spring Boot](https://projects.spring.io/spring-boot/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [MySQL](https://www.mysql.com) - Database
* [Semantic UI](https://semantic-ui.com) - User interface framework

## Versioning

* [Git Flow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow) - For development workflow.
* [SemVer](http://semver.org/) - For the versions available.

## Author

**Leonardo Costa Gonçalves**

* [Bitbucket](https://bitbucket.org/leonardo_goncalves/)
* [Linkedin](https://www.linkedin.com/in/leonardocg/)
* [leonardocg.12@gmail.com](mailto:leonardocg.12@gmail.com)