package br.com.olx.sac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SacApplication {

	/**
	 * Application start point.
	 *
	 * @param args the application arguments (usually passed from a Java main method).
	 */
	public static void main(String[] args) {
		SpringApplication.run(SacApplication.class, args);
	}
}
