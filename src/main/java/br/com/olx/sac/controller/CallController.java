package br.com.olx.sac.controller;

import br.com.olx.sac.entity.Call;
import br.com.olx.sac.service.CallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@Controller
public class CallController {

    private final CallService callService;
    private final MessageSource messageSource;

    /**
     * Default constructor where the dependencies are injected.
     *
     * @param callService a service layer responsible to integrate with repository layer.
     * @param messageSource a resource bundle with support for the parameterization and internationalization of such
     *                      messages.
     */
    @Autowired
    public CallController(CallService callService, MessageSource messageSource) {
        this.callService = callService;
        this.messageSource = messageSource;
    }


    /**
     * Render a page with call dashboard.
     *
     * @param model A holder for model attributes.
     * @return dashboard page name.
     */
    @GetMapping("/")
    public String getDashboard(Model model) {
        List<Call> calls = callService.findAll();
        model.addAttribute("calls", calls);
        return "index";
    }

    /**
     * Render a page with form to create new calls.
     *
     * @param call Representation of an user call.
     * @return form page name.
     */
    @GetMapping("call")
    public String getAddForm(@ModelAttribute Call call) {
        return "form";
    }

    /**
     * Verify if call object has errors. If it has errors, send user back to form page to correct them. If it has no
     * error, save call object on database and redirect to dashboard page.
     *
     * @param call Representation of an user call.
     * @param result General interface that represents binding results.
     * @return form page name or redirect to dashboard page.
     */
    @PostMapping("call")
    public String createCall(@Valid Call call, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            return "form";
        }
        if (callService.save(call) != null) {
            redirectAttributes.addFlashAttribute("title", messageSource.getMessage("success.title", null, Locale.getDefault()));
            redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.save.message", null, Locale.getDefault()));
        } else {
            redirectAttributes.addFlashAttribute("title", messageSource.getMessage("error.title", null, Locale.getDefault()));
            redirectAttributes.addFlashAttribute("error", messageSource.getMessage("error.save.message", null, Locale.getDefault()));
        }
        return "redirect:/";
    }

}
