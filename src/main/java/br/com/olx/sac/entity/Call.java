package br.com.olx.sac.entity;

import br.com.olx.sac.enums.CallReason;
import br.com.olx.sac.enums.CallType;
import br.com.olx.sac.enums.Province;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;

@Entity
@Table(name = "user_call")
public class Call implements Serializable {

    private static final long serialVersionUID = -1767289522198225052L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Calendar creationDate;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CallType callType;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Province province;

    @NotNull
    @Enumerated(EnumType.STRING)
    private CallReason callReason;

    @NotEmpty
    private String message;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public CallType getCallType() {
        return callType;
    }

    public void setCallType(CallType callType) {
        this.callType = callType;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public CallReason getCallReason() {
        return callReason;
    }

    public void setCallReason(CallReason callReason) {
        this.callReason = callReason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public boolean equals(Object o) {
        return EqualsBuilder.reflectionEquals(this, o);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }
}
