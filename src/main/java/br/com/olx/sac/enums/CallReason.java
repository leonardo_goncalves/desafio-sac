package br.com.olx.sac.enums;

public enum CallReason {

    DOUBTS("Dúvidas"),
    COMPLIMENTS("Elogios"),
    SUGGESTIONS("Sugestões");

    private final String name;

    CallReason(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
