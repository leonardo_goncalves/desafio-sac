package br.com.olx.sac.enums;

public enum CallType {

    TELEPHONE("Telefone"),
    CHAT("Chat"),
    EMAIL("E-mail");

    private final String name;

    CallType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
