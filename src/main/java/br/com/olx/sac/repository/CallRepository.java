package br.com.olx.sac.repository;

import br.com.olx.sac.entity.Call;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CallRepository extends CrudRepository<Call, Long> {

    /**
     * Retrieve all calls from database ordered by creation date descending.
     *
     * @return list of calls.
     */
    List<Call> findAllByOrderByCreationDateDesc();
}
