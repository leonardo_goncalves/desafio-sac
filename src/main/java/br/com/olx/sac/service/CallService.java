package br.com.olx.sac.service;

import br.com.olx.sac.entity.Call;
import br.com.olx.sac.repository.CallRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class CallService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CallService.class);

    private final CallRepository callRepository;

    /**
     * Default constructor where the dependencies are injected.
     *
     * @param callRepository a repository layer responsible to integrate with database.
     */
    @Autowired
    public CallService(CallRepository callRepository) {
        this.callRepository = callRepository;
    }

    /**
     * Persist a new call on repository.
     *
     * @param call Representation of an user call.
     * @return persisted call or null if it throws an error.
     */
    public Call save(Call call) {
        try {
            call.setCreationDate(Calendar.getInstance());
            return callRepository.save(call);
        } catch (DataAccessException e) {
            LOGGER.error("Error on saving a new call [{}]", call, e);
        }
        return null;
    }

    /**
     * Retrieve all calls from the repository.
     *
     * @return list of calls or null if it throws an error.
     */
    public List<Call> findAll() {
        try {
            return callRepository.findAllByOrderByCreationDateDesc();
        } catch (DataAccessException e) {
            LOGGER.error("Error on fetching calls", e);
        }
        return null;
    }

}
