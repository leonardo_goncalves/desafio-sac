INSERT INTO user_call (call_reason, call_type, creation_date, message, province) VALUES
('DOUBTS', 'TELEPHONE', '2017-08-17 17:10:17', 'Como faço pra entregar os produtos que vendi?', 'RJ'),
('COMPLIMENTS', 'CHAT', '2017-08-17 14:16:21', 'Melhor site para se encontrar o que preciso.', 'SP'),
('SUGGESTIONS', 'EMAIL', '2017-07-21 15:02:47', 'Poderia fazer a pesquisa por proximidae usando o smartphone', 'MG'),
('DOUBTS', 'CHAT', '2017-08-15 08:31:15', 'Como faço pra recuperar minha conta?', 'RJ'),
('COMPLIMENTS', 'CHAT', '2017-08-16 09:45:35', 'Gostaria de agradecer pelo rápido atendimento.', 'PE'),
('SUGGESTIONS', 'EMAIL', '2017-08-17 10:26:47', 'Me notifiquem pelo App quando estiver próximo de local onde vende produtos que favoritei.', 'SP'),
('DOUBTS', 'TELEPHONE', '2017-08-19 10:38:58', 'Como faço para recuperar minha senha?', 'AM');