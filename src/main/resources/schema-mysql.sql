--
-- Current Database: 'sacdb'
--

CREATE DATABASE IF NOT EXISTS sacdb;

USE sacdb;

--
-- Table structure for table 'user_call'
--

DROP TABLE IF EXISTS user_call;

CREATE TABLE user_call (
  id bigint NOT NULL AUTO_INCREMENT,
  call_reason varchar(20) NOT NULL,
  call_type varchar(20) NOT NULL,
  creation_date datetime DEFAULT NULL,
  message varchar(255) NOT NULL,
  province varchar(2) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Create user for application and set privileges
--
CREATE USER 'olxuser'@'%' IDENTIFIED BY 'olxpass';
GRANT ALL PRIVILEGES ON sacdb.* TO 'olxuser'@'%';
FLUSH PRIVILEGES;