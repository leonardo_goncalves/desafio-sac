$('.message .close').on('click', function() {
  $(this).closest('.message').transition('fade');
});

$('.ui.form').form({
  fields: {
    callType : {
      identifier: 'callType',
      rules : [
        {
          type: 'empty',
          prompt : '{name} deve ser selecionado.'
        }
      ]
    },
    province : {
      identifier : 'province',
      rules : [
        {
          type : 'empty',
          prompt : '{name} deve ser selecionado.'
        }
      ]
    },
    callReason : {
      identifier: 'callReason',
      rules : [
        {
          type: 'empty',
          prompt : '{name} deve ser selecionado.'
        }
      ]
    },
    message : {
      identifier: 'message',
      rules : [
        {
          type: 'empty',
          prompt : '{name} deve ser preenchido.'
        },
        {
          type: 'maxLength[255]',
          prompt : '{name} não pode ser maior que {ruleValue} caracteres.'
        }
      ]
    },
  }
});