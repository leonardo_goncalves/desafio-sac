package br.com.olx.sac;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SacApplicationTests {

	/**
	 * Default test to check if Spring Beans have been created.
	 */
	@Test
	public void contextLoads() {
	}

}
