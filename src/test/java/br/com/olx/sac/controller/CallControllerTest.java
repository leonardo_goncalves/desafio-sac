package br.com.olx.sac.controller;

import br.com.olx.sac.entity.Call;
import br.com.olx.sac.enums.CallReason;
import br.com.olx.sac.enums.CallType;
import br.com.olx.sac.enums.Province;
import br.com.olx.sac.repository.CallRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class CallControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CallRepository callRepository;

    /**
     * Test the flow when the {@link CallRepository#findAllByOrderByCreationDateDesc()} returns an empty list.
     * It should propagated an empty list to the user.
     *
     * @throws Exception
     */
    @Test
    public void shouldNotRetrieveCalls() throws Exception {
        ArrayList<Call> calls = new ArrayList<>();

        Mockito.when(callRepository.findAllByOrderByCreationDateDesc()).thenReturn(calls);

        MvcResult mvcResult = mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andReturn();

        Map<String, Object> model = mvcResult.getModelAndView().getModel();
        List<Call> retrievedCalls = (List<Call>) model.get("calls");

        Assert.assertTrue(retrievedCalls.isEmpty());
        Assert.assertEquals(new ArrayList<Call>(), retrievedCalls);
    }

    /**
     * Test the flow when the {@link CallRepository#findAllByOrderByCreationDateDesc()} returns an list.
     * It should propagated an the same list to the user.
     *
     * @throws Exception
     */
    @Test
    public void shouldRetrieveCalls() throws Exception {
        Call call = new Call();
        call.setCallType(CallType.TELEPHONE);
        call.setProvince(Province.RJ);
        call.setCallReason(CallReason.COMPLIMENTS);
        call.setMessage("Parabéns pelo ótimo trabalho");
        ArrayList<Call> calls = new ArrayList<>();
        calls.add(call);

        Mockito.when(callRepository.findAllByOrderByCreationDateDesc()).thenReturn(calls);

        MvcResult mvcResult = mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("index"))
                .andReturn();

        Map<String, Object> model = mvcResult.getModelAndView().getModel();
        List<Call> retrievedCalls = (List<Call>) model.get("calls");

        Assert.assertFalse(retrievedCalls.isEmpty());
        Assert.assertEquals(calls, retrievedCalls);
    }

    /**
     * Test the flow when the user request a form page.
     * It should return the form page name for Spring resolve it into an HTML.
     *
     * @throws Exception
     */
    @Test
    public void shouldRetrieveCallForm() throws Exception {
        mockMvc.perform(get("/call"))
                .andExpect(status().isOk())
                .andExpect(view().name("form"));
    }

    /**
     * Test the flow when the {@link CallRepository#save(Object)} saves an {@link Call} object.
     * It should redirect user to main page and show a success message.
     *
     * @throws Exception
     */
    @Test
    public void shouldCreateCallWithAllFields() throws Exception {
        Call call = new Call();
        call.setCallType(CallType.TELEPHONE);
        call.setProvince(Province.RJ);
        call.setCallReason(CallReason.COMPLIMENTS);
        call.setMessage("Parabéns pelo ótimo trabalho");

        Mockito.when(callRepository.save(Mockito.any(Call.class))).thenReturn(call);

        mockMvc.perform(post("/call")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("callType", "TELEPHONE")
                .param("province", "RJ")
                .param("callReason", "COMPLIMENTS")
                .param("message", "Parabéns pelo ótimo trabalho"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"))
                .andExpect(flash().attribute("title", "Parabéns!"))
                .andExpect(flash().attribute("success","Seu chamado foi registrado com sucesso!"));
    }

    /**
     * Test the flow when the {@link CallRepository#save(Object)} cannot save an {@link Call} object.
     * It should redirect user to main page and show an error message.
     *
     * @throws Exception
     */
    @Test
    public void shouldNotCreateCallWithAllFields() throws Exception {
        Mockito.when(callRepository.save(Mockito.any(Call.class))).thenReturn(null);

        mockMvc.perform(post("/call")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("callType", "TELEPHONE")
                .param("province", "RJ")
                .param("callReason", "COMPLIMENTS")
                .param("message", "Parabéns pelo ótimo trabalho"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"))
                .andExpect(flash().attribute("title", "Atenção!"))
                .andExpect(flash().attribute("error", "Houve um erro ao tentar registrar seu chamado!"));
    }

    /**
     * Test the flow when the form was send incorrectly.
     * It should keep the user at the form page and show the errors.
     *
     * @throws Exception
     */
    @Test
    public void shoulNotCreateCallWithInvalidFields() throws Exception {
        mockMvc.perform(post("/call")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("message", ""))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors())
                .andExpect(view().name("form"));
    }

}