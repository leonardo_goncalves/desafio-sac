package br.com.olx.sac.service;

import br.com.olx.sac.entity.Call;
import br.com.olx.sac.enums.CallReason;
import br.com.olx.sac.enums.CallType;
import br.com.olx.sac.enums.Province;
import br.com.olx.sac.repository.CallRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jca.cci.CannotCreateRecordException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CallServiceTest {

    @Autowired
    private CallService callService;

    @MockBean
    private CallRepository callRepository;

    /**
     * Test the flow when the {@link CallRepository#save(Object)} saves an {@link Call} object.
     * It should return the same call that was passed to save.
     *
     * @throws Exception
     */
    @Test
    public void save() throws Exception {
        Call call = new Call();
        call.setCallType(CallType.TELEPHONE);
        call.setProvince(Province.RJ);
        call.setCallReason(CallReason.COMPLIMENTS);
        call.setMessage("Parabéns pelo ótimo trabalho");

        Mockito.when(callRepository.save(call)).thenReturn(call);
        Call save = callService.save(call);

        Assert.assertEquals(call, save);
    }

    /**
     * Test the flow when the {@link CallRepository#save(Object)} try to save an {@link Call} object and throws an
     * {@link DataAccessException}.
     * It should catch the exception and return null.
     *
     * @throws Exception
     */
    @Test
    public void saveThrowsException() throws Exception {
        Call call = new Call();
        call.setCallType(CallType.TELEPHONE);
        call.setProvince(Province.RJ);
        call.setCallReason(CallReason.COMPLIMENTS);
        call.setMessage("Parabéns pelo ótimo trabalho");

        Mockito.when(callRepository.save(call)).thenThrow(DataIntegrityViolationException.class);
        Call save = callService.save(call);

        Assert.assertNull(save);
    }

    /**
     * Test the flow when the {@link CallRepository#findAllByOrderByCreationDateDesc()} returns an filled list.
     * It should not be empty or null.
     *
     * @throws Exception
     */
    @Test
    public void findAll() throws Exception {
        Call call = new Call();
        call.setCallType(CallType.TELEPHONE);
        call.setProvince(Province.RJ);
        call.setCallReason(CallReason.COMPLIMENTS);
        call.setMessage("Parabéns pelo ótimo trabalho");
        ArrayList<Call> calls = new ArrayList<>();
        calls.add(call);

        Mockito.when(callRepository.findAllByOrderByCreationDateDesc()).thenReturn(calls);
        List<Call> list = callService.findAll();

        Assert.assertNotNull(list);
        Assert.assertFalse(list.isEmpty());
    }

    /**
     * Test the flow when the {@link CallRepository#findAllByOrderByCreationDateDesc()} and throws an
     * {@link EmptyResultDataAccessException}.
     * It should catch the exception and return null.
     *
     * @throws Exception
     */
    @Test
    public void findAllThrowsException() throws Exception {
        Call call = new Call();
        call.setCallType(CallType.TELEPHONE);
        call.setProvince(Province.RJ);
        call.setCallReason(CallReason.COMPLIMENTS);
        call.setMessage("Parabéns pelo ótimo trabalho");
        ArrayList<Call> calls = new ArrayList<>();
        calls.add(call);

        Mockito.when(callRepository.findAllByOrderByCreationDateDesc()).thenThrow(EmptyResultDataAccessException.class);
        List<Call> list = callService.findAll();

        Assert.assertNull(list);
    }

    /**
     * Test the flow when the {@link CallRepository#findAllByOrderByCreationDateDesc()} returns an empty list.
     * It should not be filled or null.
     *
     * @throws Exception
     */
    @Test
    public void findEmptyList() throws Exception {
        ArrayList<Call> calls = new ArrayList<>();

        Mockito.when(callRepository.findAllByOrderByCreationDateDesc()).thenReturn(calls);
        List<Call> list = callService.findAll();

        Assert.assertNotNull(list);
        Assert.assertTrue(list.isEmpty());
    }

}